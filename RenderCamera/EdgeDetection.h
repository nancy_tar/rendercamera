//
//  EdgeDetection.h
//  Filters_OpenCV
//
//  Created by Admin on 10.05.15.
//  Copyright (c) 2015 tarasova_aa. All rights reserved.
//

#ifndef __Filters_OpenCV__EdgeDetection__
#define __Filters_OpenCV__EdgeDetection__

class EdgeDetection
{
public:
    EdgeDetection();
    
    //! Processes a frame and returns output image
    bool processFrame(const cv::Mat& inputFrame, cv::Mat& outputFrame);
    
private:
    cv::Mat grayImage;
    cv::Mat edges;
    
    cv::Mat grad_x, grad_y;
    cv::Mat abs_grad_x, abs_grad_y;
    
    cv::Mat dst;
    cv::Mat dst_norm, dst_norm_scaled;
    
    bool m_showOnlyEdges;
    std::string m_algorithmName;
    
    // Canny detector options:
    int m_cannyLoThreshold;
    int m_cannyHiThreshold;
    int m_cannyAperture;
    
    // Harris detector options:
    int m_harrisBlockSize;
    int m_harrisapertureSize;
    double m_harrisK;
    int m_harrisThreshold;
};

#endif /* defined(__Filters_OpenCV__EdgeDetection__) */
