//
//  ImageManager.h
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 15/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <opencv2/opencv.hpp>
#import "VideoFrame.h"


@interface ImageManager : NSObject


+ (cv::Mat ) createMatFromImageBuffer:(CVImageBufferRef) imageBuffer;

@end
