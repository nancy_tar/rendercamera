//
//  CVImageUtil.m
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 15/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//



#import "ImageManager.h"
#import <opencv2/opencv.hpp>
#import "opencv2/imgcodecs/ios.h"

@interface ImageManager()
@property (nonatomic)CVImageBufferRef imageBuffer;
@property (nonatomic)CGColorSpaceRef csrColorSpace;
@property (nonatomic)uint8_t *baseAddress;
@property (nonatomic)size_t sztBytesPerRow;
@property (nonatomic)size_t sztWidth;
@property (nonatomic)size_t sztHeight;
@property (nonatomic)CGContextRef cnrContext;
@property (nonatomic)CGImageRef imrImage;
@end

@implementation ImageManager


+ (cv::Mat ) createMatFromImageBuffer:(CVImageBufferRef) imageBuffer{
    
    /*Lock image buffer*/
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    
    // Collect some information required to extract the frame.
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    int bytesPerRow = (int)CVPixelBufferGetBytesPerRow(imageBuffer);
    int height = (int)CVPixelBufferGetHeight(imageBuffer);
    int width = (int)CVPixelBufferGetWidth(imageBuffer);
    
    // Extract the frame, convert it to grayscale, and shove it in _frame.
    cv::Mat bgraFrame(height, width, CV_8UC4, baseAddress, bytesPerRow);
    
    //cv::Mat frame = Mat (int rows, int cols, int type, const Scalar &s)
    /* Unlock image buffer*/
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    return bgraFrame;

}


@end