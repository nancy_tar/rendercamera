//
//  VideoFrame.h
//  AR Camera
//
//  Created by Anastasia Tarasova on 21/03/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

#ifndef VideoFrame_h
#define VideoFrame_h


#include <stddef.h>

struct VideoFrame
{
    size_t width;
    size_t height;
    size_t bytesPerRow;
    
    unsigned char * rawPixelData;
    
    //size_t width;
    //size_t height;
    //size_t bytesPerRow;
    //void * rawPixelData;
};

#endif /* VideoFrame_h */