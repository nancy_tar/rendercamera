//
//  OpenCVWrapper.m
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 06/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

#import "OpenCVWrapper.h"
#import "CalibrationWrapper.h"
#import "UIImage+OpenCV.h"

@implementation OpenCVWrapper


-(id)init {
    if ( self = [super init] ) {
        self.calibrator = [[CalibrationWrapper alloc]init];
    }
    return self;
}
@end

