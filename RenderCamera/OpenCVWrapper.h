//
//  OpenCVWrapper.h
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 06/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CalibrationWrapper.h"
//IMPORTANT! THIS FILE HAS TO BE PURE OBJECTIVE_C

@interface OpenCVWrapper : NSObject

@property (nonatomic,strong) CalibrationWrapper *calibrator;

@end

