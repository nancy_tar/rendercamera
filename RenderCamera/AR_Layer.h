//
//  AR_Layer.h
//  Filters_OpenCV
//
//  Created by Admin on 10.05.15.
//  Copyright (c) 2015 tarasova_aa. All rights reserved.
//

#import <Foundation/Foundation.h>

////////////////////////////////////////////////////////////////////

#import "EAGLView.h"
#import "CameraCalibration.hpp"
#import "GeometryTypes.hpp"
#import "VideoFrame.h"

@interface SimpleVisualizationController : NSObject
{
    EAGLView * m_glview;
    GLuint m_backgroundTextureId;
    std::vector<Transformation> m_transformations;
    CameraCalibration m_calibration;
    CGSize m_frameSize;
}

-(id) initWithGLView:(EAGLView*)view calibration:(CameraCalibration) calibration frameSize:(CGSize) size;

-(void) drawFrame;
-(void) updateBackground:(VideoFrame) frame;
-(void) setTransformationList:(const std::vector<Transformation>&) transformations;

@end
